﻿import csv
import pandas as pd
from pandas.io import sql
import psycopg2
import nltk
import numpy as np
import re

from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
from os import path

APP_ROOT = path.dirname(path.abspath(__file__))

_stop_words = None
_dir = path.join(APP_ROOT, "clf_service\\static\\ml")

def main():
    global _stop_words

    _stop_words = open_sw(_dir + "\\stop_words.txt")

    data = prepare_data()
    
    vec = HashingVectorizer(n_features=10000, ngram_range=(1, 2))
    X = vec.fit_transform(data.html_text.tolist()).toarray()

    joblib.dump(vec, _dir + "\\vec\\vec_plus.pkl")

    clf = LogisticRegression(class_weight='auto')
    clf.fit(X, data.category_id)

    joblib.dump(clf, _dir + "\\model\\model_plus.pkl")
        
    pass



def open_sw(filename):
    result = []
    with open(filename, 'rb') as csvfile:
        lines = csv.reader(csvfile, delimiter=',')
        for line in lines:
            for word in line:
                result.append(word)
    return set(result)

def prepare_data():

    _username = 'postgres'
    _password = '123456'
    _hostname = 'localhost'
    _database = 'webdb_plus'

    _sqlquery = "SELECT p.id, p.url, p.html_text, p.title, p.header, p.footer, \
                 p.meta_keywords, p.meta_description, \
                 c.category_id \
                 FROM public.pages AS p \
                 INNER JOIN public.category_page AS c ON p.id = c.page_id \
                 WHERE error = 200 AND p.html_text <> '' AND p.title NOT ILIKE '%404%'"

    conn = psycopg2.connect(host=_hostname, user=_username, password=_password, database=_database)

    print("Connect to DB. Fetching data...")

    data = sql.read_sql(_sqlquery, conn, index_col='id')

    print "Data is here. Let's charge it..."
    data.html_text = data.title + ' ' + data.html_text + ' ' + data.header + ' ' + \
                     data.footer + ' ' + data.meta_keywords + ' ' + data.meta_description

    
    _sqlquery_select_tag_a = "SELECT a.page_id, string_agg(value, ' . ') as link \
                               FROM tag_a as a \
                               WHERE value <> '' \
                               GROUP BY a.page_id"

    data_anchors = sql.read_sql(_sqlquery_select_tag_a, conn, index_col='page_id')
    data = pd.merge(data, data_anchors, right_index=True, left_index=True)
    data.html_text = data.html_text + ' ' + data.link

    print "All batteries are here. Make category for me..."

    print("OK! Make it clean...")

    total = len(data)

    for index in range(len(data)):
        txt = data.html_text.iloc[index]
        data.html_text.iloc[index] = filter_text(txt, True)
        if index % 100 == 0:
            print "{:.2%}".format(float(index)/len(data))
    
    print("Well done!")

    return data

# some remake from kaggle
def filter_text(text, is_lem=False):
    text = text.lower()
    letters_only = re.sub("[^a-zA-Z]", " ", text) 
    
    words = letters_only.lower().split()
    

    meaningful_words = []
    for w in words:
        w = w.strip()
        if w == "":
            continue
        w = stem(w, is_lem)
        length = len(w)
        if w not in _stop_words and length >= 3 and length < 25:
            meaningful_words.append(w)

    return " ".join(meaningful_words)



def stem(word, is_lem):
    stem = word
    regexp = r'^(.*?)(ing|ly|ed|ious|ies|ive|es|s|ment)?$'
    stem, suffix = re.findall(regexp, word)[0]
    if is_lem:
        wnl = nltk.WordNetLemmatizer()
        stem = wnl.lemmatize(stem)
    return stem


if __name__ == "__main__":
    main()
