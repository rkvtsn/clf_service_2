﻿from clf_service import models, db
from clf_service.classify import _CATEGORIES


db.create_all()

if db.session.query(models.Category).count() == 0:
    cat = [models.Category(title=x) for x in _CATEGORIES]
    db.session.add_all(cat)
    db.session.commit()