﻿from flask.ext.wtf import Form
from wtforms import TextField, SelectField, HiddenField
from wtforms.validators import Required, NumberRange


class PickUrlForm(Form):
    url = TextField('url', validators=[Required()])
    proxy_ip = TextField('proxy_ip')
    proxy_type = TextField('proxy_type')

#[validators.Regexp(r'^[\w.@+-]+$'), validators.Length(min=4, max=25)]


class FeedbackResultForm(Form):
    page = HiddenField('page_id')
    category = SelectField(u'Category :', coerce=int, validators=[NumberRange(min=1, max=10)])