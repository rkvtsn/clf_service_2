﻿"""
Routes and views for the flask application.
"""

from flask import render_template, flash, redirect, request, send_from_directory, url_for
from datetime import datetime, timedelta
from clf_service import app, db, models, classify
from .page_parser import Parser
from .forms import PickUrlForm, FeedbackResultForm
from decimal import Decimal
import os
import re
from werkzeug.exceptions import HTTPException

#TODO: before_request: get User instance

_MAX_REQ_COUNT = 100
_DAYS_TO_REST = 1

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/', methods=['GET','POST'])
def index():
    """Renders the main page."""
    #if app.vec == None or app.model == None:
    #    return render_template('main.html',
    #                           title="Main",
    #                           loading=True)

    last_words = "" #u" Maybe you need to use proxy."
    pages = db.session.query(models.Page).filter(models.Page.status==True).order_by(models.Page.id.desc()).limit(10).all()
    for p in pages:
        p.labels = []
        for page_category in p.categories:
            category = classify.CATEGORIES[page_category.category_id - 1] # `list` starts with `0`
            proba = round(page_category.proba, 2)
            p.labels.append((category, proba))
        p.labels = sorted(p.labels, key=lambda x: x[1], reverse=True) 

    user = user_instance()
    if user == None:
        flash('You have reached limit of requests. Please try again tomorrow.')
        return render_template('main.html',
                               title="Main",
                               pages=pages,
                               user=user,
                               form=None)

    form = PickUrlForm()
    proxy = None

    if form.validate_on_submit():
        if user.req_count >= _MAX_REQ_COUNT:
            raise HTTPException(500)
        parser = Parser(form.url.data)
        if not parser.isValid():
            flash(u"Error: Wrong url! Please check url and try again")
            return render_template('main.html',
                                   title="Main",
                                   pages=pages,
                                   user=user,
                                   form=form)
        
        proxy_exist = form.proxy_type and form.proxy_ip
        if proxy_exist:
            proxy = { form.proxy_type.data : form.proxy_ip.data, "https" : form.proxy_ip.data }
        response = parser.req(proxy)
        if response == None:
            if not proxy_exist:
                last_words = u""
            flash(u"Error: We have some problems with connection!" + u" Please try again later." + last_words)
            return render_template('main.html',
                                    title="Main",
                                    pages=pages,
                                    user=user,
                                    form=form)
        
        try:
            modified_str = response.info()['last-modified']
            modified_str = modified_str[modified_str.index(',')+2 : modified_str.rindex(' ')]
            modified = datetime.strptime(modified_str, "%d %b %Y %H:%M:%S")
        except:
            modified = datetime.now() - timedelta(days=1)

        page = db.session.query(models.Page).filter(models.Page.url==parser.url, models.Page.status==True).order_by(models.Page.id.desc()).first()
        

        if page == None or modified > page.added:
            user.req_count += 1
            user.req_time = datetime.utcnow()
            db.session.commit()

            id = parser.parse()
            if id == None:
                flash(u"Error: We have some problems with connection! Please try again later.")
                return render_template('main.html',
                                    title="Main",
                                    pages=pages,
                                    user=user,
                                    form=form)
            # bug with encoding... so we load it from DB and who-knows-how-it-works
            page = db.session.query(models.Page).filter(models.Page.id==id).order_by(models.Page.id.desc()).first()
            if page == None:
                raise HTTPException(500)
            #flash("Text proba: \"%s\"" % classify.page_to_text(page)[0][:400]) # WARN! here is check for translation

            page.content_length = len(parser.html) # not all servers will give headers with 'content-length'
            labels, translate_success = classify.predict(page,
                                      as_labels=True,
                                      proxy_options=proxy)
            if translate_success == False:
                flash("Translation services are down")
            page_categories = []
            for label, proba in labels:
                category_id = classify.CATEGORIES.index(label) + 1 # `id's` in SQL starts with `1`
                page_categories.append(models.CategoryPage(proba=proba,
                                                           category_id=category_id,
                                                           page_id=page.id))

            db.session.add_all(page_categories)
            page.status = True # marked as classified
            db.session.commit()
        #else:
        #    #if already exists in db
        #    #page_id = page.id
        #    pass

        page_id = page.id
                            
        return redirect(url_for('categories', id=page_id))

    return render_template('main.html',
                           title="Main",
                           pages=pages,
                           user=user,
                           form=form)


@app.route('/<int:id>/', methods=['GET', 'POST'])
def categories(id):
    
    page = db.session.query(models.Page).filter(models.Page.id==id, models.Page.status==True).order_by(models.Page.id.desc()).first()
    if page == None:
        return redirect(url_for('not_found_error'))
    
    
    page.labels = []
    for page_category in page.categories:
        category = classify.CATEGORIES[page_category.category_id - 1] # `list` starts with `0`
        proba = round(page_category.proba, 2)
        page.labels.append((category, proba))
    page.labels = sorted(page.labels, key=lambda x: x[1], reverse=True) 

    user = user_instance()
    if user == None or (db.session.query(models.UserCategory).filter(models.UserCategory.page_id==page.id, models.UserCategory.user_id==user.id).first() != None):
        return render_template('categories.html',
                               user=user,
                               page=page,
                               form=None,
                               title=page.url)
    
    form = FeedbackResultForm()
    
    form.category.choices = [(classify.CATEGORIES.index(t) + 1, t) for t in classify.CATEGORIES if t != page.labels[0][0]]
    form.page.data = page.id

    if form.validate_on_submit():
        if user.req_count > _MAX_REQ_COUNT:
            return redirect(url_for('index'))
        category_id = form.category.data
        page_id = form.page.data

        page_exist = db.session.query(models.Page.id).filter(models.Page.id==page_id).first() != None
        
        if user == None or page_exist == False or not str(category_id).isdigit():
            return redirect(url_for('not_found_error'))
        
        user_category = models.UserCategory(page_id=page_id, category_id=category_id, user_id=user.id)

        user.req_count += 1
        user.req_time = datetime.utcnow()

        db.session.add(user_category)
        db.session.commit()
        return redirect(url_for('thanks'))

    return render_template('categories.html',
                           user=user,
                           form=form,
                           page=page,
                           title=page.url)


@app.route('/thanks', methods=['GET'])
def thanks():
    user = user_instance()
    return render_template('thanks.html', 
                           title="Thank you for your contribution",
                           user=user)


@app.route('/about', methods=['GET', 'POST'])
def about():
    APP_ROOT = os.path.dirname(os.path.abspath(__file__))
    requirements = u", ".join(classify.open_sw(os.path.join(APP_ROOT, "../requirements.txt")))
    requirements = re.sub(r"==\d*\.?\d?\.?\d?\.?\d", u"", requirements)
    return render_template('about.html',
                           libraries=requirements,
                           categories=classify.CATEGORIES,
                           title="About")


@app.route('/make', methods=['GET','POST'])
def make_db():
    db.create_all()

    if db.session.query(models.Category).count() == 0:
        cat = [models.Category(title=x) for x in classify.CATEGORIES]
        db.session.add_all(cat)
        db.session.commit()

    return render_template('contact.html',
                           title="")



@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html', title="Page not found"), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html', error=error, title="Application error"), 500



def user_instance():   
    now = datetime.utcnow()

    usr_ip = request.remote_addr

    user = db.session.query(models.User) \
                     .filter(models.User.ip == usr_ip) \
                     .order_by(models.User.id.desc()).first()

    if user == None:
        user = models.User(req_time=now, req_count=0, ip=usr_ip)
        db.session.add(user)
        db.session.commit()

    if now - user.req_time > timedelta(days=_DAYS_TO_REST) and user.req_count != 0:
        user.req_count = 0
        db.session.commit()

    if user.req_count >= _MAX_REQ_COUNT:
        return None
    
    return user