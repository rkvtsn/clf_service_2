# coding: utf-8
import json
import requests
import six
import warnings
import logging


class ArgumentOutOfRangeException(Exception):
    def __init__(self, message):
        self.message = message.replace('ArgumentOutOfRangeException: ', '')
        super(ArgumentOutOfRangeException, self).__init__(self.message)


class TranslateApiException(Exception):
    def __init__(self, message, *args):
        self.message = message.replace('TranslateApiException: ', '')
        super(TranslateApiException, self).__init__(self.message, *args)


class Translator(object):
    
    base_url = "http://api.microsofttranslator.com/V2/Ajax.svc"

    def __init__(
            self, client_id, client_secret,
            scope="http://api.microsofttranslator.com",
            grant_type="client_credentials", app_id=None, debug=False, proxy_options=None):
    
        if app_id is not None:
            warnings.warn("""app_id is deprected since v0.4.
            See: http://msdn.microsoft.com/en-us/library/hh454950
            """, DeprecationWarning, stacklevel=2)
        
        self.proxy_options = proxy_options
        self.client_id = client_id
        self.client_secret = client_secret
        self.scope = scope
        self.grant_type = grant_type
        self.access_token = None
        self.debug = debug
        self.logger = logging.getLogger("microsofttranslator")
        if self.debug:
            self.logger.setLevel(level=logging.DEBUG)

    def get_access_token(self):
        args = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'scope': self.scope,
            'grant_type': self.grant_type
        }
        response = requests.post(
            'https://datamarket.accesscontrol.windows.net/v2/OAuth2-13',
            data=args,
            proxies=self.proxy_options
        ).json()

        self.logger.debug(response)

        if "error" in response:
            raise TranslateApiException(
                response.get('error_description', 'No Error Description'),
                response.get('error', 'Unknown Error')
            )
        return response['access_token']

    def call(self, path, params):
        if not self.access_token:
            self.access_token = self.get_access_token()

        resp = requests.get(
            "/".join([self.base_url, path]),
            params=params,
            headers={'Authorization': 'Bearer %s' % self.access_token},
            proxies=self.proxy_options
        )
        resp.encoding = 'UTF-8-sig'
        rv = resp.json()

        if isinstance(rv, six.string_types) and \
                rv.startswith("ArgumentOutOfRangeException"):
            raise ArgumentOutOfRangeException(rv)

        if isinstance(rv, six.string_types) and \
                rv.startswith("TranslateApiException"):
            raise TranslateApiException(rv)

        if isinstance(rv, six.string_types) and \
                rv.startswith(("ArgumentException: "
                               "The incoming token has expired")):
            self.access_token = None
            return self.call(path, params)
        return rv

    def translate(
            self, text, to_lang, from_lang=None,
            content_type='text/plain', category='general'):
        params = {
            'text': text.encode('utf8'),
            'to': to_lang,
            'contentType': content_type,
            'category': category,
        }
        if from_lang is not None:
            params['from'] = from_lang
        return self.call("Translate", params)

    def translate_array(self, texts, to_lang, from_lang=None, **options):
        options = {
            'Category': "general",
            'Contenttype': "text/plain",
            'Uri': '',
            'User': 'default',
            'State': ''
        }.update(options)
        params = {
            'texts': json.dumps(texts),
            'to': to_lang,
            'options': json.dumps(options),
        }
        if from_lang is not None:
            params['from'] = from_lang

        return self.call("TranslateArray", params)

    def get_languages(self):
        return self.call('GetLanguagesForTranslate', '')

    def detect_language(self, text):
        params = {
            'text': text.encode('utf8')
        }
        return self.call('Detect', params)