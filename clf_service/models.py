#!/usr/bin/python
# -*- coding: utf-8 -*-
from clf_service import db
from sqlalchemy.dialects.mysql import LONGTEXT

## TODO: remove categories from DB tables


class Domain(db.Model):
    __tablename__ = 'domains'
    id = db.Column(db.Integer, db.Sequence('domain_id_seq'), primary_key=True)    
    url = db.Column(db.Text, nullable=False)
    pages = db.relationship("Page", backref="domain", lazy="dynamic")

class Page(db.Model):
    __tablename__ = 'pages'
    id = db.Column(db.Integer, db.Sequence('page_id_seq'), primary_key=True)
    url = db.Column(db.Text, nullable=False)
    html_code = db.Column(LONGTEXT)
    html_text = db.Column(LONGTEXT)
    title = db.Column(db.Text)
    meta_keywords = db.Column(db.Text)
    meta_description = db.Column(db.Text)
    tag_h = db.Column(db.UnicodeText)
    header = db.Column(db.Text)
    footer = db.Column(db.Text)
    added = db.Column(db.DateTime)
    status = db.Column(db.Boolean)

    domain_id = db.Column(db.Integer, db.ForeignKey('domains.id'), nullable=False)
    tag_a_list = db.relationship("TagA", backref="page", lazy="dynamic")
    tag_img_list = db.relationship("TagImg", backref="page", lazy="dynamic")
    categories = db.relationship("CategoryPage")

#class Relative(db.Model):
#    __tablename__ = 'relatives'
#    id = db.Column(db.Integer, db.Sequence('relatives_id_seq'), primary_key=True)
#    parent_id = db.Column(db.Integer, db.ForeignKey('pages.id'), primary_key=True)
#    child_id = db.Column(db.Integer, db.ForeignKey('pages.id'), primary_key=True)
#    parent = db.relationship("Page", foreign_keys='Relative.parent_id')
#    child = db.relationship("Page", foreign_keys='Relative.parent_id')
    
class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, db.Sequence('user_category_id_seq'), primary_key=True)
    req_count = db.Column(db.Integer)
    req_time = db.Column(db.DateTime)
    ip = db.Column(db.String(255))

class UserCategory(db.Model):
    __tablename__ = 'user_category'
    id = db.Column(db.Integer, db.Sequence('user_category_id_seq'), primary_key=True)
    user_id = db.Column(db.Integer)
    category_id = db.Column(db.Integer)
    page_id = db.Column(db.Integer)

class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column(db.Integer, db.Sequence('category_id_seq'), primary_key=True)
    title = db.Column(db.Text, nullable=False)

class CategoryPage(db.Model):
    __tablename__ = 'category_page'
    page_id = db.Column(db.Integer, db.ForeignKey('pages.id'), primary_key=True, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'), primary_key=True, nullable=False)
    proba = db.Column(db.Float(2,2)) # XX.YY
    child = db.relationship("Category")

class TagImg(db.Model):
    __tablename__ = 'tag_img'
    id = db.Column(db.Integer, db.Sequence('tag_img_id_seq'), primary_key=True)
    src = db.Column(db.Text)
    alt = db.Column(db.Text)
    page_id = db.Column(db.Integer, db.ForeignKey('pages.id'), nullable=False)

class TagA(db.Model):
    __tablename__ = 'tag_a'
    id = db.Column(db.Integer, db.Sequence('tag_a_id_seq'), primary_key=True)
    value = db.Column(db.Text)
    href = db.Column(db.Text)
    page_id = db.Column(db.Integer, db.ForeignKey('pages.id'), nullable=False)
