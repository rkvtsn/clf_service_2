﻿"""
The flask application package.
"""
from flask.ext.sqlalchemy import SQLAlchemy
from flask import Flask


application = Flask(__name__)
app = application
app.config.from_object('config')
db = SQLAlchemy(app)


if not app.debug:
    import logging
    from logging.handlers import RotatingFileHandler
    file_handler = RotatingFileHandler('debug.log', 'a', 1 * 1024 * 1024, 10)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('startup')

from clf_service import views, models, classificator
