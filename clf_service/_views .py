﻿"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template, flash, redirect
from clf_service import app
from forms import PickUrlForm
from page_parser import Parser
import classify


@app.route('/', methods=['GET','POST'])
def index():
    """Renders the main page."""
    form = PickUrlForm()
    labels = None
    if form.validate_on_submit():
        parser = Parser(form.url.data)
        if not parser.isValid():
            flash("Bad url")
        else:
            req = parser.req()
            if req == None:
                flash("Something wrong with web page")
            else:
                txt = parser.parse()
                #predict = classify.predict(txt)
                #labels = classify.make_labels(predict)
                #flash(classify.make_labels(predict))
                flash(txt)
    #return redirect(url_for('index'))
    return render_template(
        'main.html',
        title="Main",
        labels=labels,
        form=form
    )


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    #db.session.rollback()
    return render_template('500.html', error=error), 500


#@app.route('/home')
#def home():
#    """Renders the home page."""
#    return render_template(
#        'index.html',
#        title='Home Page',
#        year=datetime.now().year,
#    )

#@app.route('/contact')
#def contact():
#    """Renders the contact page."""
#    return render_template(
#        'contact.html',
#        title='Contact',
#        year=datetime.now().year,
#        message='Your contact page.'
#    )

#@app.route('/about')
#def about():
#    """Renders the about page."""
#    return render_template(
#        'about.html',
#        title='About',
#        year=datetime.now().year,
#        message='Your application description page.'
#    )


