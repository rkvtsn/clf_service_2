﻿from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn import cross_validation
from sklearn import ensemble
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.svm import SVC


class MyClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, categories, directory):
        self.atomars = load_atomars(categories, directory)
        self.referee = joblib.load(directory + "referee.pkl")
        self.referee.probability = True # Some bug in joblib :( again
        self.categories = categories
    
    def dig(self, x):
        return int(x * 10)
    
    #def fit(self, X, y):
    #    if self.referie == None:
    #        self.referee = LogisticRegression()
    #        self.referee.fit(self._L_1_zip_proba(X), y)
    #    return self
    
    def predict(self, X):
        return self.referee.predict(self._L_1_zip_proba(X))

    def predict_proba(self, X):
        return self.referee.predict_proba(self._L_1_zip_proba(X))
    
    def _L_1_proba(self, X):
        pred = None
        data = []
        for category in self.categories:
            res = []
            pred = self.atomars[category].predict_proba(X)
            pred_len = len(pred)
            for i in range(pred_len):
                res.append(self.dig(pred[i][1]))
            data.append(res)
        return data
    
    def _L_1_zip_proba(self, X):
        return zip(*self._L_1_proba(X))

def load_atomars(categories, directory=''):
    atomars = {  }
    for category in categories:
        atomars[category] = joblib.load(directory + category+'_.pkl')
    return atomars    