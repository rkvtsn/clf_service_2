# -*- coding: utf-8 -*-
from nltk import WordNetLemmatizer
import csv
import re
from flask import url_for
import urllib2
from goslate import Goslate
import textwrap
from os import path
from tld import get_tld

from translate_yandex import YandexTranslate
from translate_bing import Translator

from clf_service import app, classificator

import numpy as np

from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib


#TODO: rework make_labels() !!! to add_confidence


# if content-length less than _CONTENT_MIN_SIZE we give _P_UNKNOWN_BY_SIZE to add_confidence 
_CONTENT_MIN_SIZE = 10000
_P_UNKNOWN_BY_SIZE = 0.5
# if black_list contains url we give _P_BLACK_LIST to add_confidence
_P_BLACK_LIST = 0.3
_MAX_CONFIDENCE = 89


CATEGORIES = [
'alcohol',
'drugs',
'gambling',
'adults',
'weapons',
'violence',
'smoking',
'suicide',
'terrorism',
'unknown',
'education',
'shopping',
'finance',
'sport']



APP_ROOT = path.dirname(path.abspath(__file__))

print 'Load clf...'
model = classificator.MyClassifier(list(CATEGORIES), path.join(APP_ROOT, "ml/model/"))
vec = joblib.load(path.join(APP_ROOT, "ml/vec/vec.pkl"))
print "Clf has been loaded"

_ADDITION_CATEGORIES = { "portal" : 0, "news" : 0 }

CATEGORIES += [x for x in _ADDITION_CATEGORIES.keys()]

def load_black_list():
    black_list = { }
    for category in CATEGORIES:
        pages = []
        with open(path.join(APP_ROOT, "ml/domains/" + category), 'r') as f:
            pages = [line.rstrip('\n') for line in f]
        black_list[category] = set(pages)
    return black_list

black_list = load_black_list()

#region stopwords
def open_sw(filename):
    result = []
    with open(filename, 'rb') as csvfile:
        lines = csv.reader(csvfile, delimiter=',')
        for line in lines:
            for word in line:
                result.append(word)
    return result

stopwords = open_sw(path.join(APP_ROOT, "ml/stop_words.txt"))
stopwords_news = open_sw(path.join(APP_ROOT, 'ml/stop_words_news.txt'))
stopwords_portal = ['mail', 'user', 'sign', 'login', 'logon', 'search']
#endregion


def add_label(p, proba):
    if p > _MAX_CONFIDENCE / 100.0:
        p = _MAX_CONFIDENCE / 100.0
    value = 0
    print "p_* : " + str(p)
    for i in range(len(proba)):
        offset = proba[i] * p
        value += offset
        proba[i] -= offset
        if proba[i] < 0:
            proba[i] = 0
    proba = np.append(proba, value)
    
    return proba

#@add_confidence
def add_confidence(title, p, proba):
    index = CATEGORIES.index(title)
    if proba[index] >= _MAX_CONFIDENCE:
        return proba
    value = 0
    # in all proba we take offset 'p'% for category by index
    for i in range(len(proba)):
        if index == i:
            continue
        offset = proba[i] * p
        value += offset
        proba[i] -= offset
        if proba[i] < 0:
            proba[i] = 0
    proba[index] = proba[index] + value
    return proba

def make_labels(proba):
    cat = CATEGORIES
    
    prediction = dict(zip(cat, proba))
    s = sorted(prediction.items(), key=lambda x:x[1], reverse=True)
    labels = [(label, round((p*100),2)) for (label, p) in s]

    return labels


def is_category(text, vec, step=1):
    X_1 = vec.fit_transform([text]).toarray()
    
    if X_1[0][0] <= step:
        return 0
    true = 0
    for x in X_1[0][1:]:
        if x >= step:
            true+=1

    proba = round(float(true) / len(X_1[0]) * 100, 2)
    
    return proba

def get_confidence(category, proba):
    return proba[CATEGORIES.index(category)]

def predict(page, as_labels=False, proxy_options=None):
    
    text, translate_success = page_to_text(page, proxy_options)
    text = pre_filter_text(text)

    # additional classificators
    p_news = is_category(text, CountVectorizer(vocabulary=stopwords_news), step=2)
    p_portal = is_category(text, CountVectorizer(vocabulary=stopwords_portal))

    p_portal = p_portal + p_news/2
    if p_portal > _MAX_CONFIDENCE:
        p_portal = _MAX_CONFIDENCE
    if p_news == 0 and p_portal < _MAX_CONFIDENCE:
        p_portal = 0

    _ADDITION_CATEGORIES["news"] = p_news
    _ADDITION_CATEGORIES["portal"] = p_portal

    print "news " + str(p_news)
    print "portal " + str(p_portal)

    text = filter_text(text=text, stopwords=set(stopwords), is_lem=True)
    X = vec.transform([text]).toarray()
    predict = model.predict_proba(X)[0]

    #region @add_confidence block
    for category, value in _ADDITION_CATEGORIES.iteritems():
        print category + str(value)
        predict = add_label(value/100.0, predict)

    print predict

    if page.content_length < _CONTENT_MIN_SIZE:
        predict = add_confidence("unknown", _P_UNKNOWN_BY_SIZE, predict)

    #region @black_list block
    domain = make_domain(page.url)
    print domain
    for category, domains in black_list.iteritems():
        if domain in domains:
            print domain + "-> in black list -> " + category
            predict = add_confidence(category, _P_BLACK_LIST, predict)
            break;
    #endregion @black_list block

    #endregion @add_confidience block


    if as_labels:
        return make_labels(predict), translate_success
    
    return predict, translate_success

def make_domain(link):
    try:
        if not link.startswith('https://'):
            if not link.startswith('http://'):
                link = "http://" + link
        d = get_tld(link, as_object=True, fail_silently=True)
        domain = d.subdomain + "." + d.tld
        if domain.startswith('.'):
            domain = domain[1:]
        if domain.startswith('www'):
            domain = domain[4:]
        return domain
    except:
        return None

def pre_filter_text(text):
    text = text.lower()
    letters_only = re.sub("[^a-zA-Z]", " ", text)
    return letters_only

def filter_text(text, stopwords, is_lem=False): 
    words = text.split()

    meaningful_words = []
    for w in words:
        w = w.strip()
        if w == "":
            continue
        w = stem(w, is_lem)
        length = len(w)
        if w not in stopwords and length >= 3 and length < 25:
            meaningful_words.append(w)

    return " ".join(meaningful_words)


def stem(word, is_lem):
    stem = word
    regexp = r'^(.*?)(ing|ly|ed|ious|ies|ive|es|s|ment)?$'
    stem, suffix = re.findall(regexp, word)[0]
    if is_lem:
        wnl = WordNetLemmatizer()
        stem = wnl.lemmatize(stem)
    return stem



def translate(t, txt, lang_to, lang_from):
    data = []
    result = u""

    parts = textwrap.wrap(txt, width=10000)
    for part in parts:
        result = t._translate(part, lang_to, lang_from) # WARNING: '_translate' is injected method!
        data.append(result)
    result = " ".join(data)
    return result

def page_to_text(page, proxy_options=None):
    text = u" "
    def_lang_txt = u" "
    
    text = page.html_text
    def_lang_txt = text[:200] if len(text) > 200 else text
    def_lang_txt += (u" " + page.title)

    err = 0
    for t_service in translate_services:
        t = t_service(proxy_options)
        try:
            lang_from = t.detect(def_lang_txt)
            lang_to = "en"
            if lang_to != lang_from:
                text = translate(t, text, lang_to, lang_from)   
            break
        except:
            err += 1
            continue
    if err == len(translate_services):
        return text, False
    return text, True

def translate_google(proxy_options):
    t = Goslate()
    if proxy_options:
        proxy_handler = urllib2.ProxyHandler(proxy_options)
        proxy_opener = urllib2.build_opener(urllib2.HTTPHandler(proxy_handler),
                                    urllib2.HTTPSHandler(proxy_handler))
        t = Goslate(opener=proxy_opener)
    else:
        t = Goslate()
    t._translate = t.translate
    print "google"
    return t

def translate_bing(proxy_options):
    t = Translator("clf_service", 
                   "d0yDw3KHIeqemySuGfP60WbtGEpBEfiKrCU/u3aMm4s=", 
                   proxy_options=proxy_options)
    t.detect = t.detect_language
    t._translate = t.translate
    print "bing"
    return t

def translate_yandex(proxy_options):
    t = YandexTranslate('trnsl.1.1.20151011T230549Z.191d91bce8a7a90a.244f4a358ab9b8f392bb60a6239499804df2f828',
                        proxy_options=proxy_options)
    
    t._translate = lambda text, lang_to, lang_from: t.translate(text, lang_from+"-"+lang_to)['text'][0]
    print "yandex"
    return t

translate_services = [ translate_yandex, translate_google, translate_bing ]