#!/usr/bin/python
# -*- coding: utf-8 -*-

# «Переведено сервисом «Яндекс.Перевод»

from urllib2 import Request, urlopen, URLError, HTTPError
from urllib import quote

#import requests 

import json
import textwrap


'''
Errors => return None
TODO: change it on Exceptions
'''

class TranslateClientException(Exception): # translate errors
    pass

class TranslateServerException(Exception): # no connection
    pass

class TranslatorYandex:
    
    def next_key():
        f = false
        if self.key_id < len(self.keys):
            self.key_id = self.key_id + 1
            f = true
        else:
            self.key_id = 0
        self.key = self.keys[self.key_id]
        return f
    
    def __init__(self, keys=["trnsl.1.1.20150808T211905Z.6be37bee7442850c.6ccf16c39a563e787cb35454af6b872ea31a1a4c"]):
        self.key = keys[0]
        self.keys = keys
        self.key_id = 0
    
        
    def send_request(self, url):
        req = Request(url)
        response = None
        try:
            response = urlopen(req)
        except HTTPError as e:
            print 'Error code: ', e.code
            return None
        except URLError as e:
            print 'Reason: ', e.reason
            return None
        
        raw = response.read()
        #response = requests.get(url)
        #raw = response.text
        #print raw
        #if response.status_code != 200 or raw.strip() == "":
        #    print "error : ", response.status_code
        #    return None
    #         raise Exception()
        
        
        
        return json.loads(raw)
    
    def def_lang(self, text, length=250):
        txt = text[:length] if len(text) > length else text
        url = "https://translate.yandex.net/api/v1.5/tr.json/detect?key=%s&text=%s" \
            % (self.key, quote(txt.encode(encoding='UTF-8')))
        data = self.send_request(url)
        return data

    def translate(self, text, lang_from, lang_to="en"):  
        lang = lang_from + "-" + lang_to
        url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=%s" \
            % (self.key, quote(text.encode(encoding='UTF-8')), lang)
        data = self.send_request(url)
        return data

    def translate_auto(self, text):
        lang = def_lang(text)
        if lang == None:
            return None
        self.translate(text, lang["lang"])
        return data

    def translate_by_subtext_auto(self, text, width, lang_to='en'):
        data = []
        lang = self.def_lang(text)
        if lang == None and lang["code"] != 200:
            print "Bad lang"
            return None
        if lang['lang'] == lang_to:
            print "It's English"
            return text

        parts = textwrap.wrap(text, width=width)

        for part in parts:
            result = self.translate(part, lang["lang"], lang_to)
            if result == None or result["code"] != 200:
                print "bad connection"
                return None
            data.append(result['text'][0])

        return " ".join(data)
        
        
    def translate_by_subtext(self, text, width, lang_from, lang_to='en'):
        data = []
        lang = lang_from
        
        if lang == lang_to:
            return text

        parts = textwrap.wrap(text, width=width)

        for part in parts:
            result = self.translate(part, lang, lang_to)
            if result == None or result["code"] != 200:
                return None
            data.append(result['text'][0])
        result = " ".join(data)
        
        return result

    retry_codes = set([401, 402, 403, 404, 413])