# -*- coding: utf-8 -*-
from urllib2 import Request, urlopen, URLError, HTTPError
import urllib2
from bs4 import BeautifulSoup
import os
import ctypes

from datetime import datetime

from models import Domain, Page, TagA, TagImg
from clf_service import db

#TODO: add `tld` package

os.environ['http_proxy']='http://10.128.0.100:8080'
os.environ['https_proxy']='http://10.128.0.100:8080'

class Parser:
    error = None
    status_code = None
    url = None
    domain = None

    def __init__(self, url):
        self.url = None
        url_box = self.prepare_url(url)
        if url_box != None:
            self.url, self.domain = url_box

    def isValid(self):
        return self.url != None

    content_types_starts = tuple(['application', 'audio', 'video', 'image'])
    content_types_ends = tuple(['css', 'javascript', 'csv'])
    headers = {'User-Agent' : 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'}

    """
    Makes request to web page.
    """
    def req(self, proxy_options=None):
        if not self.isValid():
            return None

        if proxy_options:
            proxy_handler = urllib2.ProxyHandler(proxy_options)
            opener = urllib2.build_opener(proxy_handler)
            urllib2.install_opener(opener)

        req = Request(self.url, None, self.headers)

        try:
            response = urlopen(req)
        except HTTPError as e:
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            return None
        except URLError as e:
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            return None
        else:
            self.status_code = response.getcode()
            
            if self.status_code != 200:
                return None
            
            charset = u"utf-8"
            if 'content-type' in response.info():
                content_type = response.info()['content-type'].lower()
                charset_i = content_type.find('=') + 1
                if charset_i > 0:
                    charset = content_type[charset_i:]
                if content_type.startswith(self.content_types_starts) or \
                    content_type.endswith(self.content_types_ends) and \
                    not content_type.endswith('xml'):
                    return None
            
            self.charset = charset
            try:
                self.html = response.read().decode(charset)
            except:
                return None 
            return response
    


    def get_page(self):
        if not self.isValid():
            return None
        
        soup = BeautifulSoup(self.html, 'lxml')
        
        [x.extract() for x in soup.findAll(['script', 'link', 'style'])]
        
        soup = BeautifulSoup(soup.prettify(), 'lxml')
        
        text = soup.getText(separator=u' ')
        if soup.title == None: # if page has defence on parser
            return None
        title = soup.title.get_text()
        
        meta_keywords = u'#eol '.join([s['content'] for s in soup.findAll('meta', attrs={'name':'keywords'})])
        meta_description = u'#eol '.join([s['content'] for s in soup.findAll('meta', attrs={'name':'description'})])

        headers = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(lambda x: \
                    x.name == 'header' or \
                    x.class_ == 'header' or \
                    x.id == 'header' )])

        footers = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(lambda x: \
                    x.name == 'footer' or \
                    x.class_ == 'footer' or \
                    x.id == 'footer' )])
        
        #decors = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(['cite, quote, del, strike, mark, dfn'])])
        tag_h = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(['h1, h2, h3'])])
        
        #images:
        try:
            tag_img_list = [TagImg(alt=s["alt"], src=s["src"] )for s in soup.findAll(['img'])]
            tag_img = u'#eol '.join([s["alt"] for s in soup.findAll(['img'])])
        except Exception:
            tag_img_list = []
            tag_img = ""
        
        #anchors:
        try:
            tag_a_list = [TagA(href=s["href"], value=s.getText(separator=u' ')) for s in soup.findAll(['a'])]
            tag_a = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(['a'])])
        except Exception:
            tag_a_list = []
            tag_a = ""
        
        page = Page(url=self.url,
                    html_code=self.html,
                    html_text=text,
                    title=title,
                    meta_keywords=meta_keywords,
                    meta_description=meta_description,
                    tag_h=tag_h,
                    header=headers,
                    footer=footers,
                    added=datetime.utcnow(),
                    tag_a_list=tag_a_list,
                    tag_img_list=tag_img_list)
        
        return page

    def parse(self):
        page = self.get_page()
        if page == None:
            return None

        domain = db.session.query(Domain).filter(Domain.url==self.domain).first()
        if domain == None:
            domain = Domain(url=self.domain)

        domain.pages.append(page)
        db.session.add(domain)
        db.session.commit()

        return page.id
        
    def prepare_url(self, url):
        if url == None:
            return None
        
        # filter the url
        hashIndex = url.find("#")
        if hashIndex != -1:
            url = url[:hashIndex]

        url = url.strip() #lower().

        if (len(url) == 0 or
            "javascript:" in url or
            "mailto:" in url):
            return None
        
        #decorate url
        url = url[4:] if url.startswith("www.") else url
        url = url.replace(" ","%20")
        
        if not url.startswith("https:"):
            if not url.startswith("http:"):
                url = "http://" + url

        #if not url.endswith("/"):
        #    url += '/';
        if url.endswith("/"):
            url = url[:-1]
        
        #get domain
        domain = None

        # get host
        # simply text between '//' and '/' that must have a dot '.'
        doubleSlash = url.find("//") + 2

        end = url.find('/', doubleSlash)
        end =  end if end >= 0 else len(url)

        port = url.find(':', doubleSlash)
        end = port if port > 0 and port < end else end

        domain = url[doubleSlash : end]

        if "." not in domain:
            domain = None
        
        if domain == None:
            return None

        # if where any query => remove it. that's for old school sites
        queryIndex = domain.find("?")
        if queryIndex != -1:
            domain = domain[0 : queryIndex]

        # if domain contains script extension => that's not a domain
        if domain.endswith(self.notDomain):
            return None
        
        return url, domain

    notDomain = tuple([
        ".asp", ".aspx", ".php", ".html", ".htm",
        ".cgi", ".pl", ".js", ".jsp", ".shtml",
        ".shtm", ".cfml", ".cfm", ".do", ".py",
        ".jpg", ".jpeg", ".png", ".gif", ".svg", ".bmp",
        ".mp4", ".m4v", ".flv", ".psd", ".csv", ".xml",
        ".doc", ".docx", ".rtf", ".txt", ".pps", ".ppt"
    ])

    #def translate(self, t, txt, lang_to, lang_from):
    #    data = []
    #    result = u""
    #    if lang_from == lang_to:
    #        return txt

    #    parts = textwrap.wrap(txt, width=10000)

    #    for part in parts:
    #        result = t.translate(part, lang_to, lang_from)
    #        data.append(result)
        
    #    result = " ".join(data)

    #    return result
