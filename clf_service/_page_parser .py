# -*- coding: utf-8 -*-
from urllib2 import Request, urlopen, URLError, HTTPError
from bs4 import BeautifulSoup
import textwrap
from goslate import Goslate

#from db_context import DbContext
#from db_model import *


# import warnings
# warnings.simplefilter('error')

#from translate import TranslatorYandex
#import re
        

'''
    todo: add `tld` package
'''

class Parser:

    status_code = None
    url = None
    domain = None

    def __init__(self, url):
        self.url = None
        url_box = self.prepare_url(url)
        if url_box != None:
            self.url, self.domain = url_box

    def isValid(self):
        return self.url != None

    content_types_starts = tuple(['application', 'audio', 'video', 'image'])
    content_types_ends = tuple(['css', 'javascript', 'csv'])
    headers = {'User-Agent' : 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'}

#Make a Request
    def req(self):
        if not self.isValid():
            return None
        
        req = Request(self.url, None, self.headers)

        try:
            response = urlopen(req)
        except HTTPError as e:
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            return None
        except URLError as e:
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            return None
        else:
            self.status_code = response.getcode()
            
            if self.status_code != 200:
                return None
            if response.info()['content-type'].startswith(self.content_types_starts) or \
                response.info()['content-type'].endswith(self.content_types_ends) and \
                not response.info()['content-type'].endswith('xml'):
                return None
            
            self.html = response.read() #.decode("utf-8")
            
            return self.html
    
#Parse Page
    def parse(self):
        if not self.isValid():
            return None

        soup = BeautifulSoup(self.html, 'lxml')
        [x.extract() for x in soup.findAll(['script', 'link', 'style'])]
        
        soup = BeautifulSoup(soup.prettify(), 'lxml')
        
        def_lang_txt = u" "
        def_lang_txt = (soup.body.text[:200] if len(soup.body.text) > 200 else soup.body.text)
        
        if soup.title != None:
            def_lang_txt += (u" " + soup.title.get_text())
        
        t = Goslate()
        lang_from = t.detect(def_lang_txt)
        lang_to = "en"
        
        self.html = self.translate(t, soup, lang_to, lang_from)
        
        soup = BeautifulSoup(soup.prettify(), 'lxml')

        text = soup.getText(separator=u' ')
        title = soup.title.get_text()

        meta_keywords = u'#eol '.join([s['content'] for s in soup.findAll('meta', attrs={'name':'keywords'})])
        meta_description = u'#eol '.join([s['content'] for s in soup.findAll('meta', attrs={'name':'description'})])

        headers = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(lambda x: \
                    x.name == 'header' or \
                    x.class_ == 'header' or \
                    x.id == 'header' )])

        footers = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(lambda x: \
                    x.name == 'footer' or \
                    x.class_ == 'footer' or \
                    x.id == 'footer' )])
        
        decors = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(['cite, quote, del, strike, mark, dfn'])])
        tag_h = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(['h1, h2, h3'])])
        
        #images:
        try:
            tag_img = u'#eol '.join([s["alt"] for s in soup.findAll(['img'])])
        except Exception:
            tag_img = ""
        
        #anchors:
        try:
            tag_a = u'#eol '.join([s.getText(separator=u' ') for s in soup.findAll(['a'])])
        except Exception:
            tag_a = ""
        
        
        txt = (text + " " + title + " " + tag_h + " " + meta_keywords + " " + meta_description + " " + headers + " " + footers)

        return txt + " " + tag_a + " " + tag_img + " " + decors
        #return title
        #return "HeLLO"

#Prepare Url
    def prepare_url(self, url):
        if url == None:
            return None
        
        # filter the url
        hashIndex = url.find("#")
        if hashIndex != -1:
            url = url[:hashIndex]

        url = url.strip() #lower().

        if (len(url) == 0 or
            "javascript:" in url or
            "mailto:" in url):
            return None
        
        #decorate url
        url = url[4:] if url.startswith("www.") else url
        url = url.replace(" ","%20")
        
        if not url.startswith("https:"):
            if not url.startswith("http:"):
                url = "http://" + url

        #if not url.endswith("/"):
        #    url += '/';
        if url.endswith("/"):
            url = url[:-1]
        
        #get domain
        domain = None

        # get host
        # simply text between '//' and '/' that must have a dot '.'
        doubleSlash = url.find("//") + 2

        end = url.find('/', doubleSlash)
        end =  end if end >= 0 else len(url)

        port = url.find(':', doubleSlash)
        end = port if port > 0 and port < end else end

        domain = url[doubleSlash : end]

        if "." not in domain:
            domain = None
        
        if domain == None:
            return None

        # if where any query => remove it. that's for old school sites
        queryIndex = domain.find("?")
        if queryIndex != -1:
            domain = domain[0 : queryIndex]

        # if domain contains script extension => that's not a domain
        if domain.endswith(self.notDomain):
            return None
        
        return url, domain

    notDomain = tuple([
        ".asp", ".aspx", ".php", ".html", ".htm",
        ".cgi", ".pl", ".js", ".jsp", ".shtml",
        ".shtm", ".cfml", ".cfm", ".do", ".py",
        ".jpg", ".jpeg", ".png", ".gif", ".svg", ".bmp",
        ".mp4", ".m4v", ".flv", ".psd", ".csv", ".xml",
        ".doc", ".docx", ".rtf", ".txt", ".pps", ".ppt"
    ])

    def translate(self, t, soup, lang_to, lang_from):
        data = []
        result = u""
        if lang_from == lang_to:
            return soup.prettify()

        parts = textwrap.wrap(soup.prettify(), width=10000)

        for part in parts:
            result = t.translate(part, lang_to, lang_from)
            data.append(result)
        
        result = " ".join(data)

        return result
