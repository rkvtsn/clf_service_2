﻿"""
This script runs the clf_service application using a development server.
"""

from clf_service import app

application = app

if __name__ == '__main__':
    app.run(debug=False)